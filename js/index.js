$(function(){

  $("[data-toogle='tooltip']").tooltip();

  $("[data-toggle='popover']").popover();

  $(".carousel").carousel({
    interval: 2000
  });

  $('#contacto').on('show.bs.modal', function (e){
    console.log('el modal se esta mostrando');

    $('#contactobtn').removeClass('btn-outline-success');
    $('#contactobtn').addClass('btn-primary');

    $('#contactobtn').prop('disabled', true);

  });

  $('#contacto').on('shown.bs.modal', function (e){
    console.log('el modal se mostró');
  });

  $('#contacto').on('hide.bs.modal', function (e){
    console.log('el modal se oculta');
  });

  $('#contacto').on('hidden.bs.modal', function (e){
    console.log('el modal se oculto');

    $('#contactobtn').removeClass('btn-primary');
    $('#contactobtn').addClass('btn-outline-success');

    $('#contactobtn').prop('disabled', false);

  });



});